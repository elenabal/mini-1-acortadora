import webapp



formulario = """
    <form action= " " method="POST"> 
    Introduce url a acortar : <input type="text" name="url"><br>
    Introduce el recurso : <input type="text" name="short"><br>
    <input type="submit" value="Enviar">
    </form>
"""

class Shortener(webapp.webApp):
    urls ={} #diccionario vacio

    def parse(self, request):
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\n')[-1]
        return (metodo,recurso,cuerpo)

    def process(self, parsedRequest):
        metodo, recurso, cuerpo = parsedRequest

        if metodo == 'POST':
            url = (cuerpo.split('&')[0].split('=')[-1])
            short = (cuerpo.split('&')[-1].split('=')[-1])
            if url == "" or short == "":
                httpCode = "404 Not Found"
                htmlBody = "Lo siento, recurso no encuentrado."
            if "http://" in url or "https://" in url:
                url = url
            else:
                url = "http://" + url

            self.urls[short] = url  # añado en el diccionario
            httpCode = "200 OK"
            htmlBody = "<html>" \
                       "<body>" \
                       + formulario \
                       + "Lista de URLS: " \
                       + str(self.urls) + "</p>" \
                                             "</body>" \
                                             "</html>"

        if metodo == "GET":
            if recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>" + formulario + "Lista de URLS: " + str(self.urls) + "</body></html>"
            else: #me mandan /recurso
                short = recurso.split("/")[1] #me quedo con el recurso
                if short in self.urls:
                    url = self.urls[short]
                    httpCode = "302 Found"
                    htmlBody = "<html><body><meta http-equiv='refresh' content='0; URL=" + url + "'></body></html>"
                else: #si no esta el recurso en nuestro diccionario
                    httpCode = "404 Not Found"
                    htmlBody = "Lo siento, recurso no encuentrado."

        return(httpCode,htmlBody)



if __name__ == "__main__":
    testWebApp = Shortener("localhost", 1234)
